﻿using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Web.UI.Controls;

namespace WFFM.Custom.Fields
{
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public class SingleLineSessionTextField : SingleLineText
    {
        [VisualCategory("Session")]
        [VisualProperty("Session Key", 100), DefaultValue("")]
        public string SessionKey { get; set; }


        private bool _deleteKeyAfterCapture = true;

        [DefaultValue("Yes")]
        [VisualCategory("Session")]
        [VisualFieldType(typeof (Sitecore.Form.Core.Visual.BooleanField))]
        [VisualProperty("Delete Key After Capture?", 200)]
        public string DeleteKeyAfterCapture
        {
            get
            {
                return _deleteKeyAfterCapture ? "Yes" : "No";
            }
            set
            {
                _deleteKeyAfterCapture = value == "Yes";
            }
        }

        private bool _isHidden = true;

        [VisualCategory("Session")]
        [VisualFieldType(typeof (Sitecore.Form.Core.Visual.BooleanField))]
        [VisualProperty("Is Hidden?", 300), DefaultValue("Yes")]
        public string IsHidden
        {
            get
            {
                return _isHidden ? "Yes" : "No";
            }
            set
            {
                _isHidden = value == "Yes";
            }
        }

        protected override void OnLoad(System.EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SessionKey))
            {
                if (HttpContext.Current != null && HttpContext.Current.Session[SessionKey] != null)
                {
                    Text = HttpContext.Current.Session[SessionKey].ToString();

                    if (_deleteKeyAfterCapture)
                    {
                        HttpContext.Current.Session.Remove(SessionKey);
                    }
                    
                }
            }

            if (_isHidden)
            {
                Attributes["style"] = "display:none";
            }
        }
    }
}