﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;


namespace WFFM.Custom.MVC.Fields
{
    public class SingleLineSessionTextField : Sitecore.Forms.Mvc.Models.Fields.SingleLineTextField
    {
        public string SessionKey { get; set; }

        [DataType(DataType.Text)]
        public override object Value { get; set; }

        public SingleLineSessionTextField(Item item)
            : base(item)
        {
            Initialize();
        }

        private void Initialize()
        {
            KeyValuePair<string, string> deleteKeyAfterCapture =
                ParametersDictionary.FirstOrDefault(x => x.Key.ToUpper() == "DELETEKEYAFTERCAPTURE");


            KeyValuePair<string, string> isHidden =
                ParametersDictionary.FirstOrDefault(x => x.Key.ToUpper() == "ISHIDDEN");


            if (!string.IsNullOrWhiteSpace(SessionKey))
            {
                if (HttpContext.Current != null && HttpContext.Current.Session[SessionKey] != null)
                {
                    Value = HttpContext.Current.Session[SessionKey].ToString();

                    if (deleteKeyAfterCapture.Value.ToUpper() != "YES")
                    {
                        HttpContext.Current.Session.Remove(SessionKey);
                    }

                    if (isHidden.Value.ToUpper() != "YES")
                    {
                        if (!string.IsNullOrWhiteSpace(CssClass))
                        {
                            CssClass += " hidden";
                        }
                        else
                        {
                            CssClass = "hidden";
                        }

                    }
                }
            }
        }
    }
}